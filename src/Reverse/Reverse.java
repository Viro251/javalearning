
package Reverse;
import java.lang.*;

public class Reverse
{
    public String exampleString;
    public Reverse (String providedString)
    {
        exampleString = providedString;
    }

    public String Reverser()
    {
        String reversed =  new StringBuffer(exampleString).reverse().toString();
        return reversed;
    }
}
