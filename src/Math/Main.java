package Math;

public class Main {

    public static void main(String[] args)
    {
        PracticeArray _PracticeArray = new PracticeArray();
        System.out.println("Max array value: " + _PracticeArray.returnMaxValueFromArray() );
        System.out.println("Min array value: " + _PracticeArray.returnMinValueFromArray() );
        System.out.println("Sum array values: " + _PracticeArray.returnSumValuesFromArray() );
        System.out.println("Average array value: " + _PracticeArray.returnAverageFromArray() );
        System.out.println("Median array value: " + _PracticeArray.returnMedianFromArray() );
    }
}
