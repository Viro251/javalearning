package Math;
import java.util.Arrays;
import java.util.stream.*;

public class PracticeArray
{
    int[] exampleArray = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000};

    public int returnMaxValueFromArray()
    {
        int max = Arrays.stream(exampleArray).max().getAsInt();
        return max;
    }

    public int returnMinValueFromArray()
    {
        int min = Arrays.stream(exampleArray).min().getAsInt();
        return min;
    }
    public int returnSumValuesFromArray()
    {
        int sum = IntStream.of(exampleArray).sum();
        return sum;
    }

    public int returnAverageFromArray()
    {
        PracticeArray _PracticeArray = new PracticeArray();
        int _sum = _PracticeArray.returnSumValuesFromArray();
        int averege = _sum/exampleArray.length;
        return averege;
    }

    public double returnMedianFromArray()
    {
        Arrays.sort(exampleArray);
        double median = (exampleArray[exampleArray.length / 2] + exampleArray[(exampleArray.length / 2) + 1]) / 2.0;
        return median;
    }

}
