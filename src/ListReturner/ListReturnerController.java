package ListReturner;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListReturnerController {
    Scanner user_input = new Scanner(System.in);

    public void userInputToList() {
        List<UserInput> userInputList = new ArrayList<UserInput>();

        System.out.println("Please enter a value");

        for (; ; )
        {
            // Get input number to int entered Number
            int enteredNumber = (user_input.nextInt());

            //End control
            if (enteredNumber == 0000) {
                break;
            }
            // Add to entered number to list
            userInputList.add(new UserInput(enteredNumber));

            // Information log
            System.out.println("Please enter next value or enter 0000 to end");

        }
        // Print List int
        for (UserInput x : userInputList)
        {
            System.out.println(x.userInputnumber);
        }
        // Print List size
        System.out.println("Size of list is: " + userInputList.size());
    }
}
