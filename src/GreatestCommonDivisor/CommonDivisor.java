package GreatestCommonDivisor;

public class CommonDivisor

{
    public int findGCD(int number1, int number2) {
        if(number2 == 0){
            return number1;
        }
        return findGCD(number2, number1%number2);
    }
    public void printGBC(int num1, int num2)
    {
        CommonDivisor numbers = new CommonDivisor();
        System.out.println(findGCD(num1, num2));
    }

}






