package Lotto;
import java.util.Random;
import java.util.Scanner;
public class LottoController
{
    int[] betArray = new int[6];
    int[] randomArray = new int [6];
    int numberOfTrueValues = 0;
    Scanner user_input = new Scanner(System.in);

    public void addToArray()
    {
        System.out.println("Please enter a value from 1 - 49 range");

        for(int counter = 0 ; counter < 6; counter++)
        {
            betArray[counter] = Integer.parseInt(user_input.next());
            if (betArray[counter] > 49 || betArray[counter] < 1)
            {
                System.out.println("Entered value is from wrong range!");
            }
        }

        System.out.println("");
        System.out.println("Your selected numbers:");
        for (int counter = 0; counter < 6; counter++)
        {
            System.out.println(betArray[counter]);
        }

    }
    public void drawNumbers()

    {
        Random _random = new Random();
        for(int counter = 0 ; counter < 6; counter++)
        {
            randomArray[counter] = _random.nextInt(49 - 1 + 1) + 1;
        }
        System.out.println("");
        System.out.println("Randomizing numbers...");
        for (int counter = 0; counter < 6; counter++)
        {
            System.out.println(randomArray[counter]);
        }
    }

    public void compareArrays()
    {

        System.out.println("");
        System.out.println("");
        for(int i = 0; i<betArray.length; i++)
        {
            if(betArray[i]==randomArray[i])
                {
                    System.out.println("Identical values: " + betArray[i]);
                }
        }
        for(int i = 0; i<betArray.length; i++)
        {
            if(betArray[i]==randomArray[i])
            {
                numberOfTrueValues++;
            }
        }
        System.out.println("");
        System.out.println("There are: " + numberOfTrueValues + " per 6 drawn values");
    }
}


