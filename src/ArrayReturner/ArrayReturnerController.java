package ArrayReturner;

import ListReturner.UserInput;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArrayReturnerController
{
    Scanner user_input = new Scanner(System.in);
    int[] inputArray = new int[2];
    int inputNumber;
    int inputSecondNumber;
    int [] smallerThanInputNumberArray = new int[40];
    int smallerThanInputNumberByOne;
    int inputMinusOne;
    int inputNumberTwo;

int smallerByOneFromMain;


    public void isInputNumberDivisibleByTwo()
    {
        System.out.println("Please enter value");
        inputArray[0] = Integer.parseInt(user_input.next());
        inputNumber = inputArray[0];
        System.out.println("The user input number is " + inputNumber);

        if(inputNumber%2 == 0)
        {
            System.out.println("Entered number: " + inputNumber + " is divisible by 2");
        }
        else
        {
            System.out.println("Entered number isn't divisible by 2");
        }

        System.out.println("");
    }

    public void isInputNumberDivisibleByThree()
    {
        System.out.println("Please enter value");
        inputArray[0] = Integer.parseInt(user_input.next());
        inputNumber = inputArray[0];
        System.out.println("The user input number is " + inputNumber);

        if(inputNumber%3 == 0)
        {
            System.out.println("Entered number: " + inputNumber + " is divisible by 3");
        }
        else
        {
            System.out.println("Entered number isn't divisible by 3");
        }

        System.out.println("");
    }

    public void isInputNumberDivisibleByInput()
    {
        System.out.println("Please enter value");
        inputArray[0] = Integer.parseInt(user_input.next());
        inputNumber = inputArray[0];


        System.out.println("The user input number is " + inputNumber);

        if(inputNumber%inputNumber == 0)
        {
            System.out.println("Entered number: " + inputNumber + " is divisible by " + inputNumber);
        }

        else
        {
            System.out.println("Entered number isn't divisible by " + inputNumber);
        }

    }

    public void isInputNumberDivisibleBySecondInput()
    {
        System.out.println("Please enter first first value");
        inputArray[0] = Integer.parseInt(user_input.next());
        inputNumber = inputArray[0];

        System.out.println("Please enter first second value");
        inputArray[1] = Integer.parseInt(user_input.next());
        inputSecondNumber = inputArray[1];

        if(inputNumber%inputSecondNumber == 0)
        {
            System.out.println("Entered number: " + inputNumber + " is divisible by " + inputSecondNumber);
        }
        else
        {
            System.out.println("Entered number: " + inputNumber + "isn't divisible by " + inputSecondNumber);
        }

    }

    public void getAllNumbersSmallerThanInputArray()
    {
        System.out.println("Please enter value");
        inputArray[0] = Integer.parseInt(user_input.next());
        inputNumber = inputArray[0];
        System.out.println("The user input number is " + inputNumber);



       smallerThanInputNumberArray[0] = inputNumber-1;
        System.out.println( smallerThanInputNumberArray[0]);

        for (int counter = 1; counter < inputNumber; counter++)
        {
            smallerThanInputNumberArray[counter] = smallerThanInputNumberArray[counter-1]-1;
            System.out.println(smallerThanInputNumberArray[counter]);
        }


//        for (int counter = 0; counter < 10; counter++)
//        {
//            inputMinusOne = inputMinusOne - counter;
//            smallerThanInputNumberArray[counter] = inputMinusOne;
//            System.out.println(smallerThanInputNumberArray[counter]);
//        }
//




    }

    public void getAllNumbersSmallerThanInputArrayAndDivisibleByTwo()
    {
        System.out.println("Please enter value");
        inputArray[0] = Integer.parseInt(user_input.next());
        inputNumber = inputArray[0];
        System.out.println("The user input number is " + inputNumber);

        smallerThanInputNumberArray[0] = inputNumber-1;

        for (int counter = 1; counter < inputNumber; counter++)
        {
            smallerThanInputNumberArray[counter] = smallerThanInputNumberArray[counter-1]-1;
            if(smallerThanInputNumberArray[counter]%2 == 0)
            {
                System.out.println(smallerThanInputNumberArray[counter] + " is divisible by 2");

            }

        }

    }

    public void getAllNumbersSmallerThanInputArrayAndDivisibleByThree()
    {
        System.out.println("Please enter value");
        inputArray[0] = Integer.parseInt(user_input.next());
        inputNumber = inputArray[0];
        System.out.println("The user input number is " + inputNumber);

        smallerThanInputNumberArray[0] = inputNumber - 1;

        for (int counter = 1; counter < inputNumber; counter++) {
            smallerThanInputNumberArray[counter] = smallerThanInputNumberArray[counter - 1] - 1;
            if (smallerThanInputNumberArray[counter] % 3 == 0) {
                System.out.println(smallerThanInputNumberArray[counter] + " is divisible by 3");

            }

        }
    }
    public void getAllNumbersSmallerThanInputArrayAndDivisibleBySecondInput()
    {
        System.out.println("Please enter first value");
        inputArray[0] = Integer.parseInt(user_input.next());
        inputNumber = inputArray[0];
        System.out.println("The user first input number is " + inputNumber);

        System.out.println("Please enter second value");
        inputArray[1] = Integer.parseInt(user_input.next());
        inputNumberTwo = inputArray[1];
        System.out.println("The user second input number is " + inputNumberTwo);

        smallerThanInputNumberArray[0] = inputNumber - 1;

        for (int counter = 1; counter < inputNumber; counter++) {
            smallerThanInputNumberArray[counter] = smallerThanInputNumberArray[counter - 1] - 1;
            if (smallerThanInputNumberArray[counter] % inputNumberTwo == 0) {
                System.out.println(smallerThanInputNumberArray[counter] + " is divisible by " + inputNumberTwo);

            }

        }
    }

}




